//Описание методов сортировки
method_sorting = [{
    id: '1',
    title: 'сортировка пузыриком',
    method: 'sortBubble'
}]
// описание методов сортировки 
// под конкретный тип исходных данных
benchmark_data = [{
    id: '2',
    title: 'текст с новой строки',
    method: 'PreparationArray',
    id_method_sorting: '1'
}]
// описание методов сортировки
// адаптированных по конкретный тип данных
type_data = [{
    id: '3',
    title: 'вещественные числа',
    method: 'PreparationData',
    id_method_sorting: '1'
}]

onload=function(){
    button_file = new ButtonFile({
        element: document.getElementsByClassName('file-button')[0]
    });
    button = new Button({
        element: document.getElementsByClassName('button')[0],
        function: 'onClickSortButton'
    });
    select = new Select({
        element: document.getElementById('select_sorting'),
        data: method_sorting
    });
    select = new Select({
        element: document.getElementById('select_benchmark_data'),
        data: benchmark_data
    });
    select = new Select({
        element: document.getElementById('select_type_data'),
        data: type_data
    });
}

function onClickSortButton(event){
    sorting = new Sorting({
        method_sorting: method_sorting,
        benchmark_data: benchmark_data,
        type_data: type_data
    });
}