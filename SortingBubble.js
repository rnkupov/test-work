function sortBubble(data) {
    var tmp;
    for (var i = data.length - 1; i > 0; i--) {
        for (var j = 0; j < i; j++) {
            if (data[j] > data[j+1]) {
                tmp = data[j];
                data[j] = data[j+1];
                data[j+1] = tmp;
            }
        }
    }
    return data;
}

function PreparationArray(results){
    return results.file.split('\n');
}

function PreparationData(results){
    var new_date = [];
    results.file.forEach(function (item) {
        if (isNumeric(item))
            new_date.push(parseFloat(item));
        else new_date.push(item);
    })
    return new_date
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}