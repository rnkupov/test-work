function Sorting(options){
    var method_sorting =  options.method_sorting,
        benchmark_data = options.benchmark_data,
        type_data = options.type_data

    init()

    function init(event){
        getFileData().then(results => {
            return getSort(results)
        }).then(results => {
            return getBenchmarkData(results)
        }).then(results => {
            return getTypeData(results)
        }).then(results => {
            return SortingFile(results)
        }).then(results => {
            createPaginator(results);
        });
    }
    function getFileData(){
        return new Promise(function(resolve, reject) {
            var file = document.getElementsByClassName('file-button__input')[0].files[0],
            reader = new FileReader();

            reader.onload = function (event) { 
                return resolve({file: event.target.result})
            };
            reader.readAsText(file);
        })
    }
    function getSort(results){
        return new Promise(function(resolve, reject) {
            sort_id = document.getElementsByClassName('select__title')[0].getAttribute('id');
            method_sorting.forEach(function (item) {
                if (item.id === sort_id) {
                    results.sort = item
                    return resolve(results)
                }
            });
        })
    }
    function getBenchmarkData(results){
        return new Promise(function(resolve, reject) {
            sort_id = document.getElementsByClassName('select__title')[1].getAttribute('id');
            benchmark_data.forEach(function (item) {
                if (item.id === sort_id) 
                    if (results.sort.id === item.id_method_sorting) {
                        results.file = eval(item.method+'(results)')
                        return resolve(results)
                    }
                else return resolve(results)
            });
        })
    }
    function getTypeData(results){
        return new Promise(function(resolve, reject) {
            sort_id = document.getElementsByClassName('select__title')[2].getAttribute('id');
            type_data.forEach(function (item) {
                if (item.id === sort_id) 
                    if (results.sort.id === item.id_method_sorting) {
                        results.file = eval(item.method+'(results)')
                        return resolve(results)
                    }
                else return resolve(results)
            });
        });
    }
    function SortingFile(results){
        return new Promise(function(resolve, reject) { 
            return resolve(eval(results.sort.method+'(results.file)'))
        });
    }

    function createPaginator(results){
        paginator = new Paginator({
            element: document.getElementsByClassName('paginator')[0],
            data: results,
            namber: 1,
            count: 20
        });
    }
}