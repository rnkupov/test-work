function Paginator(options){
    var elem = options.element;

    init()

    function init() {
    	var data = paginate();
    	createPaginatorContent(data);
    	createPaginatorBottom();
    }
    function createPaginatorContent(data){
      	var contener = elem.firstElementChild;
      	contener.innerHTML = '';
      	data.forEach(function(item) {
	        var div = document.createElement('div');
	        div.textContent = item;
	        contener.appendChild(div);
      	});
    }
    function createPaginatorBottom(){
    	var len = Math.ceil(options.data.length / options.count),
    		bottom = elem.lastElementChild,
			contener = document.createElement('div');

    	bottom.innerHTML = '';
        bottom.appendChild(contener);

        createNavigationElement(contener, 'Начало', 1)
        createNavigationElement(contener, 'Назад', getPrevNamber())
        CreateNavigationNamber(contener)
        createNavigationElement(contener, 'Вперед', getNextNamber())
        createNavigationElement(contener, 'Конец', len)

        contener.onclick = function(event) {
		    if (!event.target.hasAttribute('data-id')) return;
		    clickPageNamber(event.target.getAttribute('data-id'))
		};
    }

    function CreateNavigationNamber(contener){
    	var interval = getDataInterval(),
	        namber_begin = interval.namber-interval.namber_begin,
	        namber_end = interval.namber+interval.namber_end;

        for (var i = namber_begin; i < namber_end; i++){
        	var page_namber = document.createElement('div');
			page_namber.textContent = i+1;
			page_namber.setAttribute('data-id', i+1);
	        contener.appendChild(page_namber);
        }
    }

    function createNavigationElement(contener, text, namber){
    	var nav_elem = document.createElement('div');
    	nav_elem.textContent = text;
    	nav_elem.setAttribute('data-id', namber);
        contener.appendChild(nav_elem);
    }

    function clickPageNamber(namber){
    	options.namber = namber;
    	init();
    }

    function paginate(){
	    var count = options.count,
	    	data = options.data,
	    	namber = options.namber;

	    if (count < data.length){
	    	begin = (namber-1) * count
		    end = begin + count
		    return data.slice(begin, end)
	    }else return data    
	}

	function getDataInterval(){
		var namber = parseInt(options.namber),
			len = Math.ceil(options.data.length / options.count),
			namber_begin = 5,
			namber_end = 5;

		if (namber - namber_begin < 1){
		  	interval = namber - namber_begin
		  	namber_end = namber_end - interval
		  	namber_begin = namber_begin + interval
		}
		if (namber + namber_end > len > 10){
		  	interval = len - (namber + namber_end)
		  	namber_end = namber_end + interval
		  	namber_begin = namber_begin - interval
		}
		if (namber + namber_end > len < 10){
			namber_end = len - 1
		}
		return {
		  	namber: namber,
		  	namber_begin: namber_begin,
		  	namber_end: namber_end
		}
	}
	function getPrevNamber(){
		var namber = parseInt(options.namber)-1;
		if (namber < 2) return 1
		else return namber
	}
	function getNextNamber(){
		var namber = parseInt(options.namber)+1,
			len = Math.ceil(options.data.length / options.count);
		if (namber > len) return len
		else return namber		
	}
}