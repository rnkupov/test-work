function Select(options){
    var elem = options.element

    init()

    function init() {
        elem.onclick = function(event) {
            if (event.target.closest('.select__title')) toggle();
            else close()            
        }
    }

    function onDocumentClick(event) {
        if (!elem.contains(event.target)) close();
    }

    function renderItems() {
        var list = document.createElement('ul');
        list.className = "select__ul";
        options.data.forEach(function(item) {
            var li = document.createElement('li');
            li.textContent = item.title;
            li.setAttribute('id', item.id);
            list.appendChild(li);

            li.onclick = function() {
                clickItem(li);
            }
        });
        elem.appendChild(list);
    } 

    function setValue(value){
        elem.firstElementChild.textContent = value.text;
        elem.firstElementChild.setAttribute('id', value.id);
    }

    function clickItem(li){
        var value = getValueItem(li);
        setValue(value);
    }

    function getValueItem(li){
        var value={
            'id': li.getAttribute('id'),
            'text': li.textContent
            };
        return value
    }  

    function open() {
        if (!elem.querySelector('ul')) 
            renderItems();
        elem.classList.add('select__open');
        document.addEventListener('click', onDocumentClick);
    };

    function close() {
        elem.classList.remove('select__open');
    };

    function toggle() {
        if (elem.classList.contains('select__open')) close();
        else open();
    };
}